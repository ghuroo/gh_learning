var empty = function (object, property) {

    if (typeof object !== 'undefined' && object !== null) { // if object is defined

        if (typeof property !== 'undefined') { // if property is defined
            return object.hasOwnProperty(property); // check if property exists in object
        }

        return true;
    } else {
        return false;
    }

};

exports.empty = empty;