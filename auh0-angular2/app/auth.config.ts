interface AuthConfiguration {
    clientID: string,
    domain: string
}

export const myConfig: AuthConfiguration = {
    clientID: 'd9zvjeaRlzqhF1QMqMZpmR97Jyal8U7n',
    domain: 'mapptus.auth0.com'
};
