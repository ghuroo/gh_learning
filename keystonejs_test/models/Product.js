var keystone = require('keystone'),
Types = keystone.Field.Types;

var Product = new keystone.List('Product');

Product.add({
    name: { type: Types.Text, required: true, index: true },
    price: { type: Types.Money, initial: true }
});

Product.register();
