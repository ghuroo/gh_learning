var expect = require('chai').expect;

describe('checkForShip', function () {
    var checkForShip = require('../models/Ship').checkForShip;

    it('should find a ship @ player\'s coordinate', function () {
        var player = {
            ships: [
                {
                    locations: [[0,0], [0,1], [9,9]]
                }
            ]
        };

        expect(checkForShip(player, [0, 0])).to.be.true;
        expect(checkForShip(player, [0, 1])).to.be.true;
        expect(checkForShip(player, [0, 9])).to.be.false;
    });
});

describe('damageShip', function () {
    var damageShip = require('../models/Ship').damageShip;

    it('should register damage on a given ship at a given location', function () {
        var ship = {
            locations: [[0, 0]],
            damage: []
        };

        damageShip(ship, [0, 0]);

        expect(ship.damage).to.not.be.empty;
        expect(ship.damage[0]).to.deep.equal([0,0]);
    });
});

describe('fire', function () {
    var fire = require('../models/Ship').fire;

    it('should record damage on a given player ship at a given coordinate', function () {

    });
});