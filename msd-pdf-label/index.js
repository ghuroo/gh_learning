var express = require('express'),
    fs = require('fs'),
    pdf = require('html-pdf'),
    pug = require('pug'),
    Promise = require('bluebird'),
    slugify = require('slugify'),
    Case = require('case'),
    _ = require('lodash');

Number.prototype.pad = function(size) {
    var s = String(this);
    while (s.length < (size || 2)) {s = "0" + s;}
    return s;
};

var app = express();

function renderHtml(element) {
    return new Promise(function(resolve, reject) {
        var html = pug.renderFile('./input/template.pug', element);

        return resolve(html);
    });
}

function createPdf(html, pdfOptions, file) {
    return new Promise(function(resolve, reject) {

        return pdf.create(html, pdfOptions).toFile(file, function(error, result) {
            if (error) reject(new Error(error));

            return resolve(result);
        });
    });
}

var pdfOptions = {
    // 'format': 'A3',
    // 'orientation': 'landscape',

    // 8cmx31cm
    'width': '878.74px',
    'height': '229.77px', // '227.77px'
    'border': '0',
    'zoom': '1'
};

app.get('/', function (req, res) {
    // var chunkIndex = parseInt(req.query.chunk) || 0;

    // console.log('iterating chunk', chunkIndex);
    
    var data = require('./input/data.json');
    var itemsPerChunk = 200;
    // var dataChunks = _.chunk(data, itemsPerChunk);
    // var chunk = dataChunks[chunkIndex];
    var chunk = data;
    var chunkIndex = 0;

    var items = [],
        limit = null;

    for (var index = 0; index < chunk.length; index++) {
        // stop if reached limit
        if (limit && index >= limit) break;

        // create element
        var element = chunk[index];

        // normalize data
        for (var key in element) {
            element[key] = Case.capital(element[key].toLowerCase());
        }

        // generate html
        var outputName = slugify(index.pad(2) + '-' + Case.kebab((element.name + ' ' + element.city)));

        // generate new file
        if (index >= itemsPerChunk && index % itemsPerChunk === 0) chunkIndex++;
        var file = '/Users/ghuroo/Desktop/pdf/'+chunkIndex.pad(2)+'/'+outputName+'.pdf';

        items.push({ element: element, file: file });
    }

    res.send('Generating pdfs, might take a while.');

    return Promise.each(items, function(item, index, length) {
        console.log('\niterating item', index, 'of', length);

        console.log('html: generating...');
        return renderHtml(item.element)
        .then(function(result) {
            item.html = result;
            console.log('html: success');

            console.log('pdf: generating...');
            return createPdf(item.html, pdfOptions, item.file);
        }).then(function() {
            console.log('pdf: success', item.file);
        }).catch(function(error) {
            Promise.reject(error);
        });
    }).then(function() {
        return true;
    }).catch(function(error) {
        console.log(error.stack);
        throw new Error(error);
    });
});

app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});
