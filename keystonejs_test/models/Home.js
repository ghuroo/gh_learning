var keystone = require('keystone'),
    Types = keystone.Field.Types;

var Base = require('./helpers/Base.js').Base;

// Home Slider
var HomeSliderContent = new keystone.List('HomeSliderContent', {
    inherits: Base,

    label: 'Slider',
    singular: "Slide",
    plurar: "Slides",

    hidden: false,
    sortable: true,
    track: true,
    defaultSort: '-createdAt',

    map: { name: "title", unique: true }
});
HomeSliderContent.add({
    firstSentence: { type: Types.Text, initial: true },
    title: { type: Types.Text, initial: true, required: true },
    subTitle: { type: Types.Text, initial: true },
    description: { type: Types.Text, initial: true },
    image: { type: Types.CloudinaryImage, initial: true }
});
HomeSliderContent.defaultColumns = 'title, subTitle, image, firstSentence, description';
HomeSliderContent.register();
