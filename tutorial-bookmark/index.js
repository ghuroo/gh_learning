var models = require('./models/');

models.sequelize
    .authenticate()
    .then(function () {
        console.log('Connection successful');
    })
    .catch(function(error) {
        console.log("Error creating connection:", error);
    });

var app = require('express')(),
    authors = require('./controllers/authors'),
    books = require('./controllers/books');

app.get('/authors', authors.index);
app.get('/authors/:id', authors.show);
app.post('/authors', authors.create);
app.put('/authors', authors.update);
app.delete('/authors', authors.delete);

app.get('/books', books.index);
app.get('/books/:id', books.show);
app.post('/books', books.create);
app.delete('/books', books.delete);

app.set('port', process.env.PORT || 8000);

var port = app.get('port');

app.listen(port, function () {
    console.log("Magic happens on http://localhost:" + port);
});