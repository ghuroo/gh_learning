var http = require('http');
var https = require('https');

function printMessage (username, badgeCount, points) {
    var message = 'User ' + username + ' has ' + badgeCount + ' total badge(s) and ' + points + ' points in JavaScript';
    console.log(message);
}

function printError (error) {
    console.log(error.message);
}

function get (username) {

    var request = https.get('https://teamtreehouse.com/' + username + '.json', function (response) {
        var body = '';

        response.on('data', function (chunk) {
            body += chunk;
        });

        response.on('end', function () {
            if (response.statusCode === 200) {
                try {
                    var profile = JSON.parse(body);
                    printMessage(username, profile.badges.length, profile.points.JavaScript);
                } catch (error) {
                    //parse error
                    printError(error);
                }
            } else {
                printError({
                    message: 'Error getting data from user ' + username + ' (' + http.STATUS_CODES[response.statusCode] + ')'
                });
            }
        });

        request.on('error', printError);
    });
}

module.exports.get = get;