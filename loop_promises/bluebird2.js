var Promise = require('bluebird');

var users = [
    { id: 1, name: "Daniel Neves", animals: [ { name: "Rocky", color: "Brown" }, { name: "Fofy", color: "Brown" } ] },
    { id: 2, name: "Inês Meneses", animals: [ { name: "Pulga", color: "Black" }, { name: "Jack", color: "Beige" } ] }
];

function addPresents(user) {

    function add(animal, userID) {
        return new Promise(function(resolve, reject) {

            switch (userID) {
                case 1: animal.present = "Box"; break;
                case 2: animal.present = "Card"; break;
                default: reject('Oops, shouldn\'t be here.')
            }

            resolve(animal);
        });
    }

    var userID = user.id;
    var animals = user.animals;

    // return
    return Promise.each(animals, function(animal) {
        console.log('\n' + 'adding present to', animal.name);

        return add(animal, userID)
        .then(function(animal) {

            console.log(animal.name, 'now has a new present:', animal.present);

        });

    }).then(function(users) {
        console.log('finished adding presents');

        return Promise.resolve(user);
    });
}

function start(users) {

    // return
    return Promise.each(users, function(user) {
        console.log('\n' + 'going through', user.name);

        return addPresents(user)
        .then(function(user) {

            console.log('finished animals for', user.name);

        });

    }).then(function(users) {
        console.log('finished users');

        return Promise.resolve(users);
    });
}


start(users);
