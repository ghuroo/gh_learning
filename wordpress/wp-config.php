<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '.+!wzaW4|:[oRp|yIlJY9 UTNeGd%Z&BxMU(gF{N)IuQZ`3w*S4OC*9%On!9vhSU');
define('SECURE_AUTH_KEY',  'y)~pj?<H584)]-%0jD?[m5*id3Qk0[g=sw7>81 73c,DF.[7BwP;Z o6KGW-CbM>');
define('LOGGED_IN_KEY',    'eq^KtJ&^tYd_F1JK`<qv_/=33s:cTcR.qup,k%5LaR!S/{I$O@]D!q=T~WOVZYbB');
define('NONCE_KEY',        '2{eu^{XCn=M!O0m7]b;lRGf1u=QT0sZ%9:RUwJrt-QrOUmEaWpZll}b56WB~:t]s');
define('AUTH_SALT',        ']>T3+<`6Rj_E8w{/wxI5?z4q.[_s703i</%iwvW?kk_w JTWIA$odO.5St k6<Vo');
define('SECURE_AUTH_SALT', '<Fbn=rJ}aFxM6~.55ok5m:B5*x?yIF;n#ytT>Z)DSc#FzY^:;M^H6Kbbh#hD?1X,');
define('LOGGED_IN_SALT',   'w%6V_F=Y`[Iv(a8GpHwbYf9(]qwI=ydy+:K^Pv]z8GhpHKWbM5,S*XDK>=Mx/7Ju');
define('NONCE_SALT',       'E}Aw{?^PS|gho9Zu?CpW1(tdSgwc&Pw|`G/BLm)YQ~gjJ~n@Vk&F~|1kLnVMPHoU');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
