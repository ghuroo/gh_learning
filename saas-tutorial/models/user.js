const bcrypt = require('bcrypt-nodejs');
      // mongoose = require('mongoose'),
      // Schema = mongoose.Schema;

var User = sequelize.define('user', {
    firstName: { type: Sequelize.STRING, allowNull: false },
    lastName: { type: Sequelize.STRING },
    email: { type: Sequelize.STRING, isEmail: true },
    password: { type: Sequelize.STRING },
    active: 0,
    resetPasswordToken: { type: Sequelize.STRING },
    resetPasswordExpires: { type: Sequelize.STRING }
}, {
    paranoid: true
});

User.sync({force: true}).then(function () {
    // Table created
    return User.create({
        firstName: 'John',
        lastName: 'Hancock',
    });
});