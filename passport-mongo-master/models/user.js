
var mongoose = require('mongoose');

module.exports = mongoose.model('User', {
	local: {
		firstName: String,
		lastName: String,
		username: String,
		email: String
	},
	facebook: {
		// id: String,
		access_token: String,
		// firstName: String,
		// lastName: String,
		// email: String,
		id: String,
		firstName: String,
		lastName: String,
		ageRange: {
			min: String,
			max: String
		},
		gender: String,
		locale: String,
		timezone: String,
		link: String,
		email: String,
	},
	twitter: {
		id: String,
		token: String,
		username: String,
		displayName: String,
		lastStatus: String
	}

});
