var FacebookStrategy = require('passport-facebook').Strategy;
var User = require('../models/user');
var fbConfig = require('../fb.js');

module.exports = function(passport) {

    passport.use('facebook', new FacebookStrategy({
        clientID        : fbConfig.appID,
        clientSecret    : fbConfig.appSecret,
        callbackURL     : fbConfig.callbackUrl,
        profileFields: ['id', 'first_name', 'last_name', 'age_range', 'gender', 'locale', 'location', 'timezone', 'birthday', 'link', 'email']
    },

    // facebook will send back the tokens and profile
    function(access_token, refresh_token, profile, done) {

    	console.log('profile', profile);

		// asynchronous
		process.nextTick(function() {

			// find the user in the database based on their facebook id
	        User.findOne({ 'id' : profile.id }, function(err, user) {

	        	// if there is an error, stop everything and return that
	        	// ie an error connecting to the database
	            if (err)
	                return done(err);

				// if the user is found, then log them in
	            if (user) {
	                return done(null, user); // user found, return that user
	            } else {
	                // if there is no user found with that facebook id, create them
	                var newUser = new User();

                    // we will save the token that facebook provides to the user
                    newUser.facebook.access_token = access_token;

                    // set all of the facebook information in our user model
                    newUser.facebook.id = profile._json.id;
                    newUser.facebook.firstName = profile._json.first_name;
                    newUser.facebook.lastName = profile._json.last_name;
                    newUser.facebook.ageRange = profile._json.age_range;
                    newUser.facebook.gender = profile._json.gender;
                    newUser.facebook.locale = profile._json.locale;
                    newUser.facebook.timezone = profile._json.timezone;
                    newUser.facebook.link = profile._json.link;
                    newUser.facebook.email = profile._json.email;

                    // profile._json.id = '10154240480280686';
                    // profile._json.first_name = 'Daniel';
                    // profile._json.last_name = 'Neves';
                    // profile._json.age_range = { min: 21 };
                    // profile._json.gender = 'male';
                    // profile._json.locale = 'pt_PT';
                    // profile._json.timezone = 0;
                    // profile._json.link = 'https://www.facebook.com/app_scoped_user_id/10154240480280686/';
                    // profile._json.email = 'danielneves16@hotmail.com';

					// save our user to the database
	                newUser.save(function(err) {
	                    if (err)
	                        throw err;

	                    // if successful, return the new user
	                    return done(null, newUser);
	                });
	            }

	        });
        });

    }));

};
