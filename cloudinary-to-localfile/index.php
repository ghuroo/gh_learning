<?php

error_reporting(0);

// uploads
define( 'UPLOADS', __DIR__ . '/uploads' );

// twig
require_once 'vendor/Twig/Autoloader.php';
Twig_Autoloader::register();

// slim
require 'vendor/Slim/Slim.php';
\Slim\Slim::registerAutoloader();

// slim+twig
require_once 'vendor/Slim-Views/Twig.php';
require_once 'vendor/Slim-Views/TwigExtension.php';

// create app
$app = new \Slim\Slim(array(
	'view' => new \Slim\Views\Twig(),
	'templates.path' => 'views'
));

$view = $app->view();
$view->parserOptions = array(
    'debug' => false
);

$app->setName('test');

$app->config(array(
    'debug' => false
));

ini_set('display_errors', '1');
$app->error(function (\Exception $e) use ($app) { echo 'expression'; });

// libs
foreach (glob('libs/*.php') as $filename) { require_once $filename; }

// controllers
foreach (glob('controllers/*.php') as $filename) { require_once $filename; }

// models
foreach (glob('models/*.php') as $filename) { require_once $filename; }

// response headers
$app->response->headers->set('Access-Control-Allow-Origin', '*');
$app->response->headers->set('Access-Control-Allow-Methods', 'GET, POST');
$app->response->headers->set('Access-Control-Allow-Headers', 'X-Requested-With, X-Auth-Token, Content-Type');
$app->response->headers->set('Content-Type', 'application/json;charset=UTF-8');

// timezone
date_default_timezone_set("Europe/Lisbon");
define('DATE_FORMAT', 'Y-m-d H:i:s');

// start this biatch
$app->run();