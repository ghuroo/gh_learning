// Importing Node modules and initializing Express
const express = require('express'),
      app     = express(),
      bodyParser = require('body-parser'),
      morgan  = require('morgan'),
      config  = require('./config/main');

// Database connection
var sequelize = new Sequelize(config.database);

// Middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Start the server
const server = app.listen(config.port);
console.log('Running on: http://localhost:' + config.port);

// Setting up basic middleware for all Express requests
app.use(morgan('dev')); // Log requests to API using morgan

// Enable CORS from client-side
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Allow-Credentials");
    res.header("Access-Control-Allow-Credentials", "true");
    next();
});