var Promise = require('bluebird');
var slugify = require('slugify');

function secondFn(user) {
    return new Promise(function(resolve, reject) {
        var name = slugify(user.name).toLowerCase();

        resolve(name);
    });
}

function startFn(users) {
    console.log('\n' + 'going through each array item');

    return Promise.each(users, function(user) {
        console.log('\n' + 'starting', user.name);

        return secondFn(user)
        .then(function(result) {

            // do something with the operation result if needed
            console.log('finished array item', user.name, 'with result:', result, '\n');

        });

    }).then(function(users) {
        // this happens only after the whole array is processed
        // (result is the original array here)
        console.log('finished users');
        return Promise.resolve(users);
    });
}

var users = [
    { id: 1, 'name': 'Custódio Filipino' },
    { id: 2, 'name': 'Maria Albertina' }
];

module.exports = startFn(users);
