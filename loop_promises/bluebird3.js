var Promise = require('bluebird');

var users = [
    { id: 1, name: "Daniel Neves", animals: [ { name: "Rocky", color: "Brown" }, { name: "Fofy", color: "Brown" } ] },
    { id: 2, name: "Inês Meneses", animals: [ { name: "Pulga", color: "Black" }, { name: "Jack", color: "Beige" } ] }
];

function start(users) {

    // return
    return Promise.each(users, function(user) {
        console.log('\n' + 'going through', user.name);

            var animals = user.animals;

            // return
            return Promise.each(animals, function(animal) {
                console.log('\n' + 'adding present to', animal.name);

                var userID = user.id;

                return new Promise(function(resolve, reject) {

                    switch (userID) {
                        case 1: animal.present = "Box"; break;
                        case 2: animal.present = "Card"; break;
                        default: reject('Oops, shouldn\'t be here.')
                    }

                    console.log(animal.name, 'now has a new present:', animal.present);

                    resolve(animal);
                });

            }).then(function(animals) {

                console.log('finished animals');

                return Promise.resolve(animals);
            });

    }).then(function(users) {
        console.log('finished users');

        return Promise.resolve(users);
    });
}


start(users);
