var keystone = require('keystone'),
    User = keystone.list('User');

exports = module.exports = function(done) {

    new User.model({
        name: { first: 'Emergency', last: 'Agency' },
        email: 'admin@emergency-agency.com',
        password: '4zxI%I&S#UDNit4w',
        canAccessKeystone: true
    }).save(done);

};
