<?

$app->get('/', function () use($app) {
    $response = '';

    try {

        $url = $app->request()->params('source');
        $path = $app->request()->params('path');
        $file = $app->request()->params('file');

        if (empty($url) || empty($path) || empty($file)) {
            throw new Exception('Invalid parameters');
        }
        
        $path = UPLOADS . '/' . $path;
        $fullPath = $path . '/' . $file;

        if (!file_exists($fullPath)) {
            mkdir($path, 0777, true);

            // curl shit
            set_time_limit(0);
            //This is the file where we save the    information
            $fp = fopen($fullPath, 'w+');
            //Here is the file we are downloading, replace spaces with %20
            $ch = curl_init(str_replace(" ","%20", $url));
            curl_setopt($ch, CURLOPT_TIMEOUT, 50);
            // write curl response to file
            curl_setopt($ch, CURLOPT_FILE, $fp); 
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            // get curl response
            $stream = curl_exec($ch); 
            curl_close($ch);
            fclose($fp);
        }

        $response = $fullPath;

    } catch (Exception $e) {
        $response = 'Error: ' . $e->getMessage();
        $app->response->setStatus(500);
    }

    $app->response->write($response);
});