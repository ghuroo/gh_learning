'use sctrict';

var express = require('express'),
    utilities = require('./textUtilities.js');

var app = express();

app.get('/', function(req, res){

    var people = [
        { id: 1, name: 'Daniel' },
        { id: 2, name: 'Zé' }
    ];

    var ok = utilities.gatherNamesOf(people);

    res.json(ok);
});

app.listen(4000, function(){
    console.log("http://localhost:4000");
});