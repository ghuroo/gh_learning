var keystone = require('keystone'),
    Types = keystone.Field.Types;

var Base = new keystone.List('Base', {
    hidden: true
});

Base.add({
    language: { type: Types.Select, required: true, options: [{ value: 'pt', label:'Português' }, { value: 'en', label: 'English' }], initial: 'pt' },
    dateStart: { type: Types.Datetime, default: Date.now, required: true, initial: true, label: 'Display Date' },
    expires: { type: Types.Boolean, default: false, label: 'Expires?' },
    dateEnd: { type: Types.Datetime, dependsOn: { expires: true }, required: false, label: "Expiraton Date" },
});
Base.register();

// validate dateEnd
Base.schema.pre('validate', function(next) {
    if (this.expires && this.dateEnd && this.dateEnd <= this.dateStart) {
        next(Error('Expiration Date must be posterior to Display Date'));
    } else {
        next();
    }
});

// validate expires
Base.schema.pre('validate', function(next) {
    if (this.expires && !this.dateEnd) {
        next(Error('Expires: Expiration Date must be set'));
    } else {
        next();
    }
});

// remove dateEnd if expires is not selected
Base.schema.pre('save', function(next) {
    if (!this.expires) this.dateEnd = ''; next();
});

exports.Base = Base;
