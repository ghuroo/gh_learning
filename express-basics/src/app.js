'use strict';

var express = require('express'),
	  posts = require('./mock/posts.json'),
       libs = require('./libs.js');

var app = express();

app.use(express.static(__dirname + '/public'));

app.set('view engine', 'pug');
app.set('views', __dirname + '/templates');

app.get('/', function(req, res){
    var data = {
        title: 'Home'
    };

	res.render('index', data);
});

app.get('/blog/:id?', function(req, res){
    var data = {
        title: 'Blog',
        posts: posts
    };

    for (var index in posts) {
        if (posts[index].id == req.params.id) {
            data.post = posts[index];
            data.title = data.post.title;
        }
    }

    libs.empty(data.post) ? res.render('post', data) : res.render('blog', data);
});

app.listen(3000, function(){
	console.log("The frontend server is running on port 3000!")
});