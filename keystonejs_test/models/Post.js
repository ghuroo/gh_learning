var keystone = require('keystone'),
Types = keystone.Field.Types;

var Post = new keystone.List('Post');

Post.add({
    name: { type: Types.Text, initial: true, required: true, index: true },
    user: { type: Types.Relationship, ref: 'User' }
});

Post.register();
