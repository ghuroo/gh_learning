'use strict';

module.exports = function(sequelize, DataTypes) {
    var Book = sequelize.define('Book', {
        name: DataTypes.STRING,
        isbn: DataTypes.INTEGER,
        publicationDate: DataTypes.DATE,
        description: DataTypes.TEXT,
        authorID: DataTypes.INTEGER
    }, {
        classMethods: {
            associate: function(models) {
                // associations can be defined here
            }
        }
    });

    return Book;
};