var keystone = require('keystone'),
    Product = keystone.list('Product');

exports = module.exports = function(done) {

    new Product.model({
        name: 'Sapato',
        price: 5
    }).save(done);

};
