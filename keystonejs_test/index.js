var keystone = require('keystone');

keystone.init({

    'name': 'Testing Keystone',

    // 'favicon': 'public/favicon.ico',
    'less': 'public',
    'static': ['public'],

    'views': 'templates/views',
    'view engine': 'pug',

    'auto update': true,
    'mongo': 'mongodb://localhost/test_keystonejs',

    'session': true,
    'auth': true,
    'user model': 'User',
    'cookie secret': 'teste123',

    'cloudinary config': 'cloudinary://654647271839138:Zp0JYJ632e35F7JO8W_N5Mjc7Cg@emergency-agency',
    'cloudinary prefix': 'test_keystonejs',
    'cloudinary folders': true

});

require('./models');

keystone.set('routes', require('./routes'));

// keystone.set('nav', {
//     'users': 'users',
//     'documents & media': ['images'],
//     'home': ['HomeSliderContent']
// });

// keystone.set( "nav", {
//     'users': 'users',
//     "home": ['HomeSliderContent']
// });

keystone.start();

// |--lib
// |  Custom libraries and other code
// |
// |--models
// |  Your application's database models
// |
// |--public
// |  Static files (css, js, images, etc.) that are publicly available
// |
// |--routes
// |  |--api
// |  |  Your application's api controllers
// |  |
// |  |--views
// |  |  Your application's view controllers
// |  |
// |  |--index.js
// |  |  Initialises your application's routes and views
// |  |
// |  |--middleware.js
// |  |  Custom middleware for your routes
// |
// |--templates
// |  |--includes
// |  |  Common .jade includes go in here
// |  |
// |  |--layouts
// |  |  Base .jade layouts go in here
// |  |
// |  |--mixins
// |  |  Common .jade mixins go in here
// |  |
// |  |--views
// |  |  Your application's view templates
// |
// |--updates
// |  Data population and migration scripts
// |
// |--package.json
// |  Project configuration for npm
// |
// |--web.js
// |  Main script that starts your application
