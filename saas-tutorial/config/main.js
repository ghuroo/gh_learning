module.exports = {
    // Secret key for JWT signing and encryption
    'secret': 'teste',
    // Database connection information
    'database': 'mysql://root:root@localhost/mapptus',
    // Setting port for server
    'port': process.env.PORT || 3000
};