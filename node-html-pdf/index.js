var fs = require('fs');
var pdf = require('html-pdf');
var options = {
    "format": 'A4',
    "orientation": "portrait",
    "border": "0",
    "zoomFactor": "1",
    "zoom": "1"
};

var express = require('express');
var app = express();

app.get('/', function (req, res) {

    var html = fs.readFileSync('./template.html', 'utf8');

    res.type('pdf');

    pdf.create(html).toStream(function(err, stream){
        stream.pipe(res);
    });

});

app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});
