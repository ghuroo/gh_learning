var express = require('express');
var router = express.Router();

// middlewares
var isAuthenticated = function (req, res, next) {
	// if user is authenticated in the session, call the next() to call the next request handler
	// passport adds this method to request object. a middleware is allowed to add properties to
	// request and response objects
	if (req.isAuthenticated()) return next();

	// if the user is not authenticated then redirect him to the login page
	res.redirect('/');
};

// routes
module.exports = function(passport){

	// homepage
	router.get('/', function(req, res) {
		res.render('index', { message: req.flash('message') });
	});

	// handle generic logout
	router.get('/signout', function(req, res) {
		req.logout();
		res.redirect('/');
	});

	// LOCAL
	// local authenticated page
	router.get('/local/home', isAuthenticated, function(req, res) {
		res.render('local-home', { user: req.user });
	});

	// local handle login post
	router.post('/local/login', passport.authenticate('login', {
		successRedirect: '/local/home',
		failureRedirect: '/',
		failureFlash : true
	}));

	// local strategy signin
	router.get('/local/signin', function(req, res) {
		res.render('local-signin', { message: req.flash('message')});
	});

	// local strategy signup
	router.get('/local/signup', function(req, res) {
		res.render('local-signup', { message: req.flash('message')});
	});

	// handle local strategy registration post
	router.post('/local/signup', passport.authenticate('signup', {
		successRedirect: '/local/home',
		failureRedirect: '/local/signup',
		failureFlash : true
	}));

	// local authenticated page
	router.get('/local/home', isAuthenticated, function(req, res) {
		res.render('local-home', { user: req.user });
	});

	// FACEBOOK
	// facebook authenticated page
	router.get('/facebook/home', isAuthenticated, function(req, res) {
		res.render('facebook-home', { user: req.user });
	});

	// facebook strategy signin
	router.get('/facebook/signin',
		passport.authenticate('facebook', { scope : 'email' }
	));

	// facebook strategy callback after facebook has authenticated the user
	router.get('/facebook/signin/callback',
		passport.authenticate('facebook', {
			successRedirect : '/facebook/home',
			failureRedirect : '/'
		})
	);

	return router;
};
