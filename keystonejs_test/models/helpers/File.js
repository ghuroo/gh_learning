var keystone = require('keystone'),
    Types = keystone.Field.Types;

// Storage Adapters
var path = '/uploads/images';
var fileStorage = new keystone.Storage({
    adapter: keystone.Storage.Adapters.FS,
    fs: {
        path: keystone.expandPath('.' + path), // required; path where the files should be stored
        publicPath: '/public' + path, // path where files will be served
    }
});

// Image Template
var Image = new keystone.List('Image', {
    track: true
});
Image.add({
    name: { type: Types.Text, initial: true, required: true, unique: true },
    file: { type: Types.File, storage: fileStorage },
});
Image.defaultColumns = 'name, file';
Image.register();
