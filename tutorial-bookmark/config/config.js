module.exports = {
    development: {
        url: 'mysql://root:root@localhost/bookmark',
        dialect: 'mysql'
    },
    production: {
        url: process.env.DATABASE_URL,
        dialect: 'mysql'
    },
    staging: {
        url: process.env.DATABASE_URL,
        dialect: 'mysql'
    },
    test: {
        url: process.env.DATABASE_URL || 'postgres://root:root@localhost/bookmark_test',
        dialect: 'mysql'
    }
};