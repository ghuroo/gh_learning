var fs = require('fs');
var pdf = require('html-pdf');
var html = fs.readFileSync('./template.html', 'utf8');
var options = {
    "format": 'A4',
    "orientation": "portrait",
    "width": "600px",
    "border": "0",
    "zoom": "0.55"
};

var express = require('express');
var app = express();

app.get('/', function (req, res) {
    pdf.create(html, options).toFile('./simulacao.pdf', function(err, res2) {
        if (err) return console.log(err);

        var tempFile="./simulacao.pdf";
        fs.readFile(tempFile, function (err,data){
           res.contentType("application/pdf");
           res.send(data);
        });

    });
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
