var expect = require('chai').expect;

expect(true).to.be.true;

var titleCase = function (title) {
    var words = title.split(' ');
    var titleCasedWords = words;

    // for (var i = 0; i < words.length; i++) {
    //     titleCasedWords[i] = words[i][0].toUpperCase() + words[i].substring(1);
    // }

    titleCasedWords = words.map(function (word) {
        word = word.toLowerCase();
        word = word[0].toUpperCase() + word.substring(1);

        console.log(word);
        return word;
    });

    return titleCasedWords.join(' ');
};

// expect(titleCase('the great mouse detective')).to.be.a('string');
// expect(titleCase('a')).to.equal('A');
// expect(titleCase('the great mouse detective')).to.equal('The Great Mouse Detective');
expect(titleCase('x-MEn: firST ClAsS')).to.be.equal('X-Men: First Class');