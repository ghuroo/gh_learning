var profile = require('./profile');
var params = process.argv.slice(2);
var command = params[0];

if (command === 'get') {
    var users = params.slice(1);

    users.forEach(profile.get);
}

//part2
var unsecurePlainTextPassword = 'daniel';

var colors = require('colors');
var bcrypt = require('bcrypt');
const saltRounds = 5;
const myPlaintextPassword = unsecurePlainTextPassword;

bcrypt.genSalt(saltRounds, function(err, salt) {
    bcrypt.hash(myPlaintextPassword, salt, function(err, hash) {
        console.log(hash.grey);
    });
});