��    )      d  ;   �      �  F   �     �     �     �                    2     G  W   X     �     �     �  	   �     �     �     �  N     	   P  
   Z     e     n  !   |     �     �  \   �          !  j   '     �     �  :   �     �     �               	       
   %     0  ^  D  �   �	  "   5
     X
  &   r
     �
     �
  -   �
     �
  +     �   >     �  !   �  
     #   '      K  #   l     �  Y   �     �       
     %   &  A   L     �  )   �  �   �     |  	   �  �   �     V     l  w   �  1         2     5     8     =  4   @     u     �                      !       "                    (      $                    	                  )                                                       &                           %       '   
          #    %1$s thought on &ldquo;%2$s&rdquo; %1$s thoughts on &ldquo;%2$s&rdquo; Add to slider Choose file Comments are closed. Continue Edit Enter search keyword File is not selected Get started here It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Main Sidebar Newer Comments Next Next page Nothing Found Older Comments Pingback Please try using our search box below to look for information on the internet. Posted on Powered by Previous Previous page Ready to publish your first post? Reply Search results for: Sorry, but nothing matched your search terms. Please try again with some different keywords. Sticky post Tags: The page you are looking for might have been removed, had its name changed, or is temporarily unavailable. This is post author To add this Widgets in this area will be shown on all posts and pages. You may use these and at by in into the slider, mark it learn more tags and attributes Project-Id-Version: Green Garden
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-06-24 14:52+0300
PO-Revision-Date: 
Last-Translator: bestwebsoft.com <plugins@bestwebsoft.com>
Language-Team: bestwebsoft.com <wp_theme@bestwebsoft.com>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x;esc_attr_e
X-Poedit-Basepath: .
X-Generator: Poedit 1.8.4
X-Poedit-SearchPath-0: ..
 %1$s комментарий к &ldquo;%2$s&rdquo; %1$s комментария &ldquo;%2$s&rdquo; %1$s комментариев &ldquo;%2$s&rdquo; Добавить в слайдер Выберите файл Комментарии закрыты. Продолжить Редактировать Введите слово для поиска Файл не выбран Для начала нажмите сюда Похоже что мы не можем найти то что вы ищете. Возможно система поиска может вам помочь. Главный сайдбар Новые комментарии Далее Следующая страница Ничего не найдено Старые комментарии Pingback Пожалуйста воспользуйтесь системой поиска ниже. Опубликовано Создано Назад Предыдущая страница Готовы разместить свой первый пост? Ответить Результаты поиска для: Извините, но  по вашему запросу ничего не найдено. Попробуйте еще раз с другими ключевыми словами. Важный пост Тэги: Страница, которую вы ищете, могла быть перемещена, ей могли поменять имя, или она временно недоступна. Автор поста Чтобы добавить Виджеты в этой области будут показаны на всех постах и страницах. Вы можете использовать эти и в от в в слайдер, поставьте галочку читать далее тэги и атрибуты 