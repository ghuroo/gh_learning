var _ = require('lodash');
var fs = require('fs');

var templates = [];

templates.push(require('./templates/vlocity_cmt__BusinessProcessSLDSTemplates.json'));
templates.push(require('./templates/vlocity_cmt__SldsAngular.json'));

// for each chunk of templates
_.forEach(templates, function(chunk, index) {
	
	// for each template in this chunk
	_.forEach(chunk, function(content, key) {

		// console.log(template);

		// parse?

		// export to file
		console.log(key);
		fs.writeFile('./output/' + key, content, function(err) {
		    
		    if (err) {
		    	return console.log(err);
		    }

		    console.log(key + ' file generated.');
		});
	});
});

